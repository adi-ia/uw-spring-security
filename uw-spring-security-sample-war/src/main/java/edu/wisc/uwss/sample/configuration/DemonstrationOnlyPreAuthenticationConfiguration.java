/**
 * 
 */
package edu.wisc.uwss.sample.configuration;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import edu.wisc.doit.adi.web.PreAuthenticationSimulationServletFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.filter.DelegatingFilterProxy;


import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.configuration.development.SimulatePreAuthenticationHttpSecurityAmender;

/**
 * Demonstration-only sample {@link Configuration}.
 * This Configuration injects a Servlet {@link Filter} that is useful for development environments
 * as it allows us to simulate the behavior of a preauthentication service provider (like a SAML SP).
 * 
 * WARNING: do not enable this Filter in production environments, as all users will be authenticated
 * as the same user.
 * 
 * @author Nicholas Blair
 */
@Configuration
@Profile("edu.wisc.uwss.preauth-simulate-netid")
public class DemonstrationOnlyPreAuthenticationConfiguration {

  @Inject
  private Environment environment;

  @Inject
  private AuthenticationManager authenticationManager;
  /**
   * This method injects {@link #preAuthenticationSimulationFilter()} for demonstration purposes. You
   * will not need to inject this simulation filter in any deployed environment.
   * 
   * @param authenticationManager
   * @throws ServletException
   * @throws Exception
   */
  @Bean
  protected HttpSecurityAmender httpSecurityAmender(AuthenticationManager authenticationManager) throws ServletException, Exception {
    return new SimulatePreAuthenticationHttpSecurityAmender(preAuthenticationSimulationFilter());
  }
  /**
   * This Filter is intended to simulate pre-authentication by making every request appear to have the user attributes
   * provided in the uwss-SAMPLE.properties.
   * 
   * This Filter can be skipped by adding a query parameter to the URL, like so:
   * 
   * <pre>
   * localhost:8080/lazy?_ignorepreauth
   * </pre>
   * 
   * Note: Annotating this method with {@link Bean} will cause 2 filter instances of the same type to be
   * initialized, 1 going in the wrong spot of the filter chain. Do not annotate this method with {@link Bean}.
   * 
   * @return a {@link PreAuthenticationSimulationServletFilter} ONLY useful for demonstration purposes
   * @throws ServletException
   */
  protected Filter preAuthenticationSimulationFilter() throws ServletException {
      PreAuthenticationSimulationServletFilter filter = new PreAuthenticationSimulationServletFilter();
      filter.setRemoteUser(environment.getProperty("preauth.remoteUser"));
      filter.setAdditionalHeaders(environment.getProperty("preauth.headerNames"), environment.getProperty("preauth.headerValues"));
      return new DelegatingFilterProxy(filter) {
        
        private final List<String> IGNORED = Arrays.asList("/", "/index.html", "/favicon.ico");
        private final Logger logger = LoggerFactory.getLogger(getClass());
        /* (non-Javadoc)
         * @see org.springframework.web.filter.DelegatingFilterProxy#invokeDelegate(javax.servlet.Filter, javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
         */
        @Override
        protected void invokeDelegate(Filter delegate, ServletRequest request,
            ServletResponse response, FilterChain filterChain) throws ServletException, IOException {
          HttpServletRequest httpRequest = (HttpServletRequest) request;
          String uri = httpRequest.getRequestURI();
          String param = httpRequest.getParameter("_ignorepreauth");

          // remove CRLF to avoid CWE-93
          String cleanUri = (uri!=null) ? uri.replaceAll("([\\r\\n])", " ") : null;
          String cleanParam = (param!=null) ? param.replace("([\\r\\n])","") : null;
          logger.debug("uri={}, param={}", cleanUri, cleanParam);
          if(null != param || IGNORED.contains(uri)) {
            logger.info("skipping PreAuthenticationSimulationServletFilter, either due to '_ignorepreauth' or visiting ignore uri");
            filterChain.doFilter(request, response); 
          } else {
            super.invokeDelegate(delegate, request, response, filterChain);
          }
        }
        
      };
  }
}
