package edu.wisc.uwss.configuration.development;


import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

import javax.servlet.Filter;

import edu.wisc.uwss.configuration.HttpSecurityAmender;

/**
 * {@link HttpSecurityAmender} useful for development environments.
 *
 * @see PreAuthenticationSimulationServletFilter
 * @author Nicholas Blair
 */
public class SimulatePreAuthenticationHttpSecurityAmender implements HttpSecurityAmender{

  private final Filter preAuthenticationSimulationServletFilter;

  /**
   * @param preAuthenticationSimulationServletFilter
   */
  public SimulatePreAuthenticationHttpSecurityAmender(Filter preAuthenticationSimulationServletFilter) {
    this.preAuthenticationSimulationServletFilter = preAuthenticationSimulationServletFilter;
  }

  /**
   * {@inheritDoc}
   *
   * Injects the {@link PreAuthenticationSimulationServletFilter} into the filterchain in the
   * correct spot, so that the HttpServletRequest attributes it provides are available when the
   * preauthentication filter tries to read them.
   *
   * @param http
   * @throws Exception
   */
  @Override
  public void amend(HttpSecurity http) throws Exception {
    http.addFilterBefore(preAuthenticationSimulationServletFilter, SecurityContextPersistenceFilter.class);
  }

}
