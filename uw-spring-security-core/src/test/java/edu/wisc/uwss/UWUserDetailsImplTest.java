/**
 * 
 */
package edu.wisc.uwss;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Tests for {@link UWUserDetailsImpl}.
 * @author Collin Cudd
 */
public class UWUserDetailsImplTest {

  /**
   * Test which verifies the clone style constructor for {@link UWUserDetailsImpl} produces
   * equivalent objects but does NOT return the same instance.
   */
  @Test
  public void clone_produces_equivalant_but_not_same_instance() {
    String pvi = "pvi";
    String username = "username";
    String password = "password";
    String fullName = "fullName";
    String emailAddress = "emailAddress";
    Collection<String> uddsMembership = Collections.singletonList("udds1"); 
    String customLogoutUrl = "customLogoutUrl";
    String eppn = "eppn";
    String isisEmplid = "isisEmplid";
    String source = "source";
    
    Collection<? extends GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("role"));
    UWUserDetailsImpl original = new UWUserDetailsImpl(pvi, username, password, fullName, emailAddress, uddsMembership, authorities);
    
    original.setCustomLogoutUrl(customLogoutUrl);
    original.setEppn(eppn);
    original.setIsisEmplid(isisEmplid);
    original.setSource(source);
    original.setFirstName("Bucky");
    original.setLastName("Badger");
    
    UWUserDetailsImpl copy = new UWUserDetailsImpl(original);
    assertEquals(original, copy);
    assertTrue(Objects.equals(original, copy));
    assertFalse(original == copy);
  }
  /**
   * Add leading and trailing spaces to verify the {@link UWUserDetailsImpl#getEmailAddress()} is trimmed
   */
  @Test
  public void email_trimmed() {
    String email = "some@email.com";
    UWUserDetailsImpl user = new UWUserDetailsImpl("UW123A123", "bucky", "", "Bucky Badger", "  " + email + "     ");
    assertEquals(user.getEmailAddress(), email);
  }
  /**
   * Format email to upper case to verify the {@link UWUserDetailsImpl#getEmailAddress()} is lower case
   */
  @Test
  public void email_toLower() {
    String email = "some@email.com";
    UWUserDetailsImpl user = new UWUserDetailsImpl("UW123A123", "bucky", "", "Bucky Badger", email.toUpperCase());
    assertEquals(user.getEmailAddress(), email);
  }
  /**
   * Confirm {@link UWUserDetailsImpl#getEmailAddressHash()} is consistent regardless of email constructor argument case.
   */
  @Test
  public void emailAddressHash_consistency() {
    String email = "some@email.com";
    UWUserDetailsImpl lower = new UWUserDetailsImpl("UW123A123", "bucky", "", "Bucky Badger", email);
    UWUserDetailsImpl upper = new UWUserDetailsImpl("UW123A123", "bucky", "", "Bucky Badger", email.toUpperCase());
    
    assertEquals(lower.getEmailAddress(), upper.getEmailAddress());
    assertEquals(lower.getEmailAddressHash(), upper.getEmailAddressHash());
  }

  @Test
  public void newInstance_success() {
    UWUserDetailsImpl instance = UWUserDetailsImpl.newInstance("UW123A123", "bucky", "", "Bucky Badger", "bucky.badger@wisc.edu", Collections.<String>emptyList(), Collections.<String>emptyList());
  }
}
