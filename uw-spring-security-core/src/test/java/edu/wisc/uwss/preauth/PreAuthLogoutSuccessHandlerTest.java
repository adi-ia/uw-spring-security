/**
 * 
 */
package edu.wisc.uwss.preauth;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;

import edu.wisc.uwss.UWUserDetails;

/**
 * Tests for {@link PreAuthLogoutSuccessHandler}.
 * 
 * @author Nicholas Blair
 */
public class PreAuthLogoutSuccessHandlerTest {

  /**
   * Verify safe behavior of {@link PreAuthLogoutSuccessHandler#onLogoutSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, Authentication)}
   * for null authentication.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void onLogoutSuccess_no_authentication() throws IOException, ServletException {
    MockHttpServletRequest request = new MockHttpServletRequest();
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    PreAuthLogoutSuccessHandler handler = new PreAuthLogoutSuccessHandler();
    handler.setDefaultTargetUrl("/test");
    // try both for null Authentication
    handler.onLogoutSuccess(request, response, null);
    assertEquals("/test", response.getRedirectedUrl());
    
    request = new MockHttpServletRequest();
    response = new MockHttpServletResponse();
    // and for null Authentication#getPrincipal 
    Authentication authentication = mock(Authentication.class);
    handler.onLogoutSuccess(request, response, authentication);
    assertEquals("/test", response.getRedirectedUrl());
  }
  /**
   * Verify behavior of {@link PreAuthLogoutSuccessHandler#onLogoutSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, Authentication)}
   * for a user with a defined {@link UWUserDetails#getCustomLogoutUrl()}.
   * 
   * @throws ServletException 
   * @throws IOException 
   */
  @Test
  public void onLogoutSuccess_custom_url() throws IOException, ServletException {
    Authentication authentication = mock(Authentication.class);
    UWUserDetails userDetails = mock(UWUserDetails.class);
    when(userDetails.getCustomLogoutUrl()).thenReturn("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/");
    when(authentication.getPrincipal()).thenReturn(userDetails);
    
    MockHttpServletRequest request = new MockHttpServletRequest();
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    PreAuthLogoutSuccessHandler handler = new PreAuthLogoutSuccessHandler();
    handler.onLogoutSuccess(request, response, authentication);
    
    assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", response.getRedirectedUrl());
  }
  /**
   * Similar test to {@link #onLogoutSuccess_custom_url()}, only the URL is in a different ServletContext.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void onLogoutSuccess_custom_url_alternate_context() throws IOException, ServletException {
    Authentication authentication = mock(Authentication.class);
    UWUserDetails userDetails = mock(UWUserDetails.class);
    when(userDetails.getCustomLogoutUrl()).thenReturn("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/");
    when(authentication.getPrincipal()).thenReturn(userDetails);
    
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.setContextPath("/alternateServletContext");
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    PreAuthLogoutSuccessHandler handler = new PreAuthLogoutSuccessHandler();
    handler.onLogoutSuccess(request, response, authentication);
    
    assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", response.getRedirectedUrl());
  }
  
  /**
   * Verify behavior of {@link PreAuthLogoutSuccessHandler#onLogoutSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, Authentication)}
   * for a user with no custom logout url.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void onLogoutSuccess_default_behavior_on_empty_customUrl() throws IOException, ServletException {
    Authentication authentication = mock(Authentication.class);
    UWUserDetails userDetails = mock(UWUserDetails.class);
    when(authentication.getPrincipal()).thenReturn(userDetails);
    
    MockHttpServletRequest request = new MockHttpServletRequest();
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    PreAuthLogoutSuccessHandler handler = new PreAuthLogoutSuccessHandler();
    // in practice, the way we configure Spring Security results in a defaultTargetUrl being set to 'logout'
    handler.setDefaultTargetUrl("/foo/");
    handler.onLogoutSuccess(request, response, authentication);
    
    // PreAuthLogoutSuccessHandler#getDefaultTargetUrl is not visible
    assertEquals("/foo/", response.getRedirectedUrl());
  }
}
