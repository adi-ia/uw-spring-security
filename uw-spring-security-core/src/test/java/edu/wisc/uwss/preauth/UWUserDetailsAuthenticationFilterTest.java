/**
 * 
 */
package edu.wisc.uwss.preauth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import edu.wisc.uwss.UWUserDetails;

/**
 * Tests for {@link UWUserDetailsAuthenticationFilter}.
 * 
 * @author Nicholas Blair
 */
public class UWUserDetailsAuthenticationFilterTest {

	/**
	 * Control experiment for {@link UWUserDetailsAuthenticationFilter#getPreAuthenticatedPrincipal(HttpServletRequest)}.
	 */
	@Test
	public void getPreAuthenticatedPrincipal_control() {
		UWUserDetailsAuthenticationFilter filter = new UWUserDetailsAuthenticationFilter();
		HttpServletRequest request = mock(HttpServletRequest.class);

    when(request.getHeader("uid")).thenReturn("bbadger");
		when(request.getHeader("eppn")).thenReturn("bbadger@wisc.edu");
		when(request.getHeader("cn")).thenReturn("Bucky Badger");
		when(request.getHeader("displayName")).thenReturn("Buckingham Badger");
		when(request.getHeaders("wisceduudds")).thenReturn(Collections.enumeration(Arrays.asList("A061234")));
		when(request.getHeader("mail")).thenReturn("foo@foo.wisc.edu");
    when(request.getHeader("isMemberOf")).thenReturn("somegroup");
		UWUserDetails userDetails = filter.getPreAuthenticatedPrincipal(request);
		assertEquals("bbadger", userDetails.getUsername());
		assertEquals("bbadger@wisc.edu", userDetails.getEppn());
		assertEquals("Bucky Badger", userDetails.getFullName());
		assertEquals("Buckingham Badger", userDetails.getDisplayName());
		assertEquals("foo@foo.wisc.edu", userDetails.getEmailAddress());
		assertEquals(1, userDetails.getUddsMembership().size());
		assertEquals("A061234", userDetails.getUddsMembership().iterator().next());
    assertEquals("somegroup", userDetails.getAuthorities().iterator().next().getAuthority());
	}
	
	/**
	 * Verify behavior for {@link UWUserDetailsAuthenticationFilter#getPreAuthenticatedPrincipal(HttpServletRequest)}
	 * when no "udds" header is present. Example would be someone not employed by the University.
	 */
	@Test
	public void getPreAuthenticatedPrincipal_no_udds() {
		UWUserDetailsAuthenticationFilter filter = new UWUserDetailsAuthenticationFilter();
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		when(request.getHeader("uid")).thenReturn("bbadger");
		when(request.getHeader("displayName")).thenReturn("Buckingham Badger");
		when(request.getHeader("cn")).thenReturn("Bucky Badger");
		UWUserDetails userDetails = filter.getPreAuthenticatedPrincipal(request);
		assertEquals("bbadger", userDetails.getUsername());
		assertEquals("Bucky Badger", userDetails.getFullName());
		assertEquals("Buckingham Badger", userDetails.getDisplayName());
		assertTrue(userDetails.getUddsMembership().isEmpty());
	}
  /**
   * Verify behavior for {@link UWUserDetailsAuthenticationFilter#getPreAuthenticatedPrincipal(HttpServletRequest)}
   * when no "manifest" header is present, which is commonly true unless an the application is designed
   * specifically to consume a manifest group.
   */
  @Test
  public void getPreAuthenticatedPrincipal_no_manifestgroups() {
    UWUserDetailsAuthenticationFilter filter = new UWUserDetailsAuthenticationFilter();
    HttpServletRequest request = mock(HttpServletRequest.class);

    when(request.getHeader("uid")).thenReturn("bbadger");
	  when(request.getHeader("displayName")).thenReturn("Buckingham Badger");
	  when(request.getHeader("cn")).thenReturn("Bucky Badger");
    UWUserDetails userDetails = filter.getPreAuthenticatedPrincipal(request);
    assertEquals("bbadger", userDetails.getUsername());
	  assertEquals("Bucky Badger", userDetails.getFullName());
	  assertEquals("Buckingham Badger", userDetails.getDisplayName());
    assertTrue(userDetails.getAuthorities().isEmpty());
  }
  /**
	 * Verify behavior for {@link UWUserDetailsAuthenticationFilter#getPreAuthenticatedPrincipal(HttpServletRequest)} when
	 * no "uid" header is present. This would represent a scenario where our pre-authentication (shib) environment isn't correctly
	 * configured; we want to make sure attempts to login would fail and not accidentially leak people in. 
	 */
	@Test
	public void getPreAuthenticatedPrincipal_no_uid() {
		UWUserDetailsAuthenticationFilter filter = new UWUserDetailsAuthenticationFilter();
		HttpServletRequest request = mock(HttpServletRequest.class);
		assertNull(filter.getPreAuthenticatedPrincipal(request));
	}
}
