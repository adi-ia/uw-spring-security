/**
 * 
 */
package edu.wisc.uwss.preauth;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import edu.wisc.uwss.UWUserDetails;

/**
 * Implementation of {@link AuthenticationUserDetailsService} intended for use with
 * {@link UWUserDetailsAuthenticationFilter}.
 * 
 * @author Nicholas Blair
 */
@Profile({ "preauth", "edu.wisc.uwss.preauth" })
public class UWUserDetailsAuthenticationUserDetailsService implements
    AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

  /**
   * {@inheritDoc}
   * 
   * Expects {@link Authentication#getPrincipal()} to be a {@link UWUserDetails} instance.
   */
  @Override
  public UWUserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token)
      throws UsernameNotFoundException {
    UWUserDetails userDetails = (UWUserDetails) token.getPrincipal();
    return userDetails;
  }

}
