package edu.wisc.uwss.local;

import edu.wisc.uwss.UWUserDetails;

/**
 * Callback interface allowing downstream projects to mutate the {@link UWUserDetails} returned
 * by {@link LocalUserDetailsManagerImpl#loadUserByUsername(String)} during an authentication attempt.
 *
 * Example usage:
 *
 <pre>
 class MyLocalUsersAuthenticationAttemptCallback implements LocalUsersAuthenticationAttemptCallback<MyUWUserDetailsImpl> {

   @Autowired
   private MySomethingDao somethingDao;

   public void success(MyUWUserDetailsImpl userDetails) {
     userDetails.setMySomethingField(somethingDao.getSomethingsForUser(userDetails.getUsername());
   }
 }
 </pre>
 *
 * Consumers can register any number of Beans implementing this interface; see {@link org.springframework.core.Ordered}
 * if you need a particular order of execution.
 *
 * If instead you need to modify {@link UWUserDetails} instances during application initialization only,
 * see {@link LocalUserDetailsLoader}.
 *
 * @author Nicholas Blair
 */
public interface LocalUsersAuthenticationAttemptCallback<T extends UWUserDetails> {

  /**
   * Callback executed upon successfully loading the {@link UWUserDetails} for a username.
   *
   * @param userDetails the userDetails to modify
   */
  void success(T userDetails);
}
