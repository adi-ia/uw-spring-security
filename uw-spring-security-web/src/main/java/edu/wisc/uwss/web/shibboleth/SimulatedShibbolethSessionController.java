package edu.wisc.uwss.web.shibboleth;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import edu.wisc.uwss.UWUserDetails;

/**
 * {@link Controller} intended to simulate the JSON response from /Shibboleth.sso/Session.json,
 * using the current authenticated {@link UWUserDetails} as the principal.
 *
 * @author Nicholas Blair
 */
@Controller
@Profile("edu.wisc.uwss.simulated-shibboleth")
public class SimulatedShibbolethSessionController {

  @RequestMapping(value="/Shibboleth.sso/Session.json", method=RequestMethod.GET)
  public @ResponseBody Object shibbolethSessionProfile(@AuthenticationPrincipal Object principal, HttpServletRequest request) {

    if(principal instanceof UWUserDetails) {
      return new SimulatedResponse((UWUserDetails) principal, request.getRemoteAddr());
    } else {
      return Collections.emptyMap();
    }

  }
}
