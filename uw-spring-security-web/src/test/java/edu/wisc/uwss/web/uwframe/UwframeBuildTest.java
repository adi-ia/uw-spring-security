package edu.wisc.uwss.web.uwframe;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link UwframeBuild}
 * @author Nicholas Blair
 */
public class UwframeBuildTest {

  @Test
  public void toVersionString_empty() {
    assertEquals("", new UwframeBuild().toVersionString());
  }

  @Test
  public void toVersionString_revisionOnly() {
    assertEquals("Revision abcde1", new UwframeBuild().setBuildNumber("abcde1").toVersionString());
  }
  @Test
  public void toVersionString_release_withbranch() {
    assertEquals("Revision abcde1 (Version 1.2.3 from release-1-2 branch)",
            new UwframeBuild()
                    .setBuildNumber("abcde1")
                    .setProjectVersion("1.2.3")
                    .setScmBranch("release-1-2")
                    .toVersionString());
  }
  @Test
  public void toVersionString_release_withtag() {
    assertEquals("Revision abcde1 (Version 1.2.3)",
            new UwframeBuild()
                    .setBuildNumber("abcde1")
                    .setProjectVersion("1.2.3")
                    .setTags("1.2.3")
                    .setScmBranch("feature-branch")
                    .toVersionString());
  }
  @Test
  public void toVersionString_complete() {
    assertEquals("Revision abcde1 (Version 1.2.3-SNAPSHOT from feature-branch branch)",
            new UwframeBuild()
                    .setBuildNumber("abcde1")
                    .setProjectVersion("1.2.3-SNAPSHOT")
                    .setScmBranch("feature-branch")
                    .toVersionString());
  }
}
